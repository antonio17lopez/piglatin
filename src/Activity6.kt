fun main(){

    var word:String=""
    word= readLine().toString()
    piglatin(word)
}

fun piglatin(word:String) {

    var i = 0
    var letter: Char = ' '
    var noparar: Boolean = true
    var posicioncorte: Int = 0

    while (i < word.length && noparar == true) {
        letter = word[i]
        if (letter == 'a' || letter == 'e' || letter == 'i' || letter == 'o' || letter == 'u' || letter == 'A' || letter == 'E' || letter == 'I' || letter == 'O' || letter == 'U'  ) {
            noparar = false
            posicioncorte = i
        } else {
            i = i + 1
        }
    }
    if (posicioncorte == 0){
        println(word)
    }
    else {
        var pedazo1: String = word.subSequence(0, posicioncorte).toString()
        var pedazo2: String = word.subSequence(posicioncorte, word.length).toString()
        println(pedazo2 + "-" + pedazo1 + "ay")
    }
}